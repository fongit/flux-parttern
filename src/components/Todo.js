import React, { Component } from 'react'
import TodoStore from '../store/TodoStore';
import * as TodoActions from '../action/TodoAction'
export class Todo extends Component {
    constructor(){
        super();
        this.state={
            todos:TodoStore.getAll(),
            tasktodo:''
        }
    }
    
    componentWillMount() {
        TodoStore.on('change',()=>{
            this.setState({
                todos:TodoStore.getAll(),
            })
        })
    }
    componentWillUnmount(){
      TodoStore.removeListener('change',this.getAll())
    }
    
    
    createTodo(){
        TodoActions.createTodo(this.state.tasktodo)
    }
    handleChange=(e)=>{
        this.setState({
            [e.target.id]:e.target.value
        })
    }
  render() {
      const {todos}=this.state;
      const TodoComponents = todos.map((todo)=>{
          return <li key={todo.id}>{todo.text}</li>
      })
    return (
      <div>
          <label htmlFor="tasktodo">ADD TODO:</label>
          <input type="text" id='tasktodo' onChange={this.handleChange} value={this.state.tasktodo}/>
          <button onClick={this.createTodo.bind(this)}>Create Button</button>
        <h1>TODO LIST:</h1>
        <ul>{TodoComponents}</ul>
      </div>
    )
  }
}

export default Todo
