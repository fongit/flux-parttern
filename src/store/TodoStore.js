import {EventEmitter} from 'events'
import dispatcher from '../dispactcher/dispatcher'
import TodoAction from '../action/TodoAction'
class TodoStore extends EventEmitter{
    constructor(){
        super()
        this.todos =[
            {
                id:1283199334,
                text:'Going Shoping',
                complete:false 
            },
            {
                id:1231234234,
                text:'Play Soccer',
                complete:true 
            },
            {
                id:1231232343,
                text:'Tech a React Class',
                complete:true 
            }
        ]    
    }
    createTodo(text){
        const id = Date.now()
        this.todos.push({
            id,
            text,
            complete:false,
        })
        // Emit is used to tell the component that the store has change and it needs to update
        this.emit('change');
    }
    getAll(){
        return this.todos;
    }
    handleActions(action){
        switch(action.type){
            case 'CREATE_TODO':{
                this.createTodo(action.text);
                break
            }
            default : {
                return;
            }
        }
    }
}
const todoStore = new TodoStore();
dispatcher.register(todoStore.handleActions.bind(todoStore));
window.dispatcher = dispatcher;
// window.todoStore = todoStore;
export default todoStore;
