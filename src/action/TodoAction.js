import dispatcher from '../dispactcher/dispatcher'
export const createTodo=(text)=>{
    dispatcher.dispatch({
        type:'CREATE_TODO',
        text,
    })
}
export const deleteTodo=(id)=>{
    dispatcher.dispatch({
        type:'DELETE_TODO',
        id,
    })
}